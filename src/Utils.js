import { useRef, useEffect } from "react";

const usePrevious = function(value) {
  const ref = useRef();
  useEffect(() => ref.current = value);
  return ref.current;
}

export default usePrevious;